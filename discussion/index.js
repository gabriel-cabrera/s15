// COMMENTS in JavaScript
// - To write comments, we use two forward slash for single line comments and two forward slash and two asterisks for multi-line comments.

// This is a single line comment.
/*
	This is a multi-line comment.
	To write them, we add two asterisks inside of the forward slashes and write comments in between them.
*/ 

// VARIABLE
/*
	-Variables  contain values that can be changed over the execution time of the program.
	-To declare a variable, we use the "let" keyword.
*/

/*let productName = 'desktop computer';
console.log(productName);
productName = 'cellphone';
console.log(productName);


let	productPrice = 500;
console.log(productPrice);
productPrice = 450;
console.log(productPrice);*/


// CONSTANTS

/*
	- Use constants for values that will not change.
*/

/*const deliveryFee = 30;
console.log(deliveryFee);
*/

// DATA TYPES
/*
	1. STRINGS (str)

	- series of characters that create a word, a phrase, sentence, or anything related to "TEXT"
	- in JavaScript can be written using a single or double quote ('') and ("")
	- On other programming langauges, only the double quote can be used for creating strings.


*/

let country = 'Philippines'
let province = "Metro Manila"

// CONCATENATION
console.log(country + ', ' + province)

/*
	2. NUMBERS

	-include intergers/whole numbers, decimal numbers, fraction, exponential notations.
*/

let headCount = 26;
console.log(headCount);

let grade = 98.7;
console.log(grade);

let planetDistance = 2e10;
console.log(planetDistance);

console.log(Math.PI);

console.log("John's grade last quater is : " + grade);

/*
	3. Boolean

	- values are logical values.
	- values can contain either "True" or "False"
*/

let isMarried = false;
let isGoodConduct = true;

console.log("is Married " + isMarried);
console.log("is Good Conduct " + isGoodConduct);



// 4. OBJECTS
/*
	4.1 ARRAYS

	- a special kind of data that stores multiple values.
	- a special type of an object.
	- can store different data types but is normally used to store similar data stypes.
*/

// Syntax:
	// let/const arrayName = [ElementA, ElementB, ElementC]

let grades = [ 98.7, 92.1, 90.2, 94.6 ];
console.log(grades)

/*ARRAY TRAVERSAL
grades[0-n]
*/

let userDetails = ["John", "Smith", 32, true];
console.log(userDetails)

	// OBJECT LITERALS
	/*
		- Objects are another special kind of data type that mimics real world objects/items.
		- They are used to create complex data that contains pieces of information that are relevant to each other.
		- Every individual piece of information if called a property of the object.

		Syntax:

			let/const objectName = {
				propertyA: value;
				propertyB: value;
			}
	*/

	let personDetails = {
		fullName: 'Juan Dela Cruz',
		age: 35, 
		isMarried: false,
		contact: ['+639876543210', '024785425'],
		address: {
			houseNumber: '345',
			city: 'Manila'
		}
	}
	console.log(personDetails)

	//5. NULL
	/*
	- used to intentionally express the absence of a value.
	*/

	let spouse = null;
	console.log(spouse);

	// 6. UNDEFINED
	/*
	 - Represents the state of a variable that has been declared but without an assigned value.
	*/

		let vacationPlace;
		console.log(vacationPlace);